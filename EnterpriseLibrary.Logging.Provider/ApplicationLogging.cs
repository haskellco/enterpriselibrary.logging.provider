﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace EnterpriseLibrary.Logging
{
    public static class ApplicationLogging
    {

        public static void LogError(Exception e, string message, Category_EL category)
        {
            Logger.SetLogWriter(new LogWriterFactory().Create(), false);

            message += "\r\n" + e.Message +
                      "\r\n\r\nStack trace: " + e.StackTrace +
                      "\r\n\r\nSource: " + e.Source +
                      "\r\n\r\nSite: " + e.TargetSite +
                      "\r\n\r\n";
            if (e.InnerException != null)
            {
                message += "Inner exception:\r\n==============================" +
                           "Error Message: " + e.InnerException.Message +
                           "\r\n\r\nStack trace: " + e.InnerException.StackTrace +
                           "\r\n\r\nSource: " + e.InnerException.Source +
                           "\r\n\r\nSite: " + e.InnerException.TargetSite +
                           "\r\n\r\n";
            }

            message += "DateTime: " + DateTime.Now.ToString();

            Logger.Write(message, category.ToString());

            Logger.Writer.Dispose();
        }

        public static void LogError(Exception e, string message, Category category)
        {
            Logger.SetLogWriter(new LogWriterFactory().Create(), false);

            message += "\r\n" + e.Message +
                      "\r\n\r\nStack trace: " + e.StackTrace +
                      "\r\n\r\nSource: " + e.Source +
                      "\r\n\r\nSite: " + e.TargetSite +
                      "\r\n\r\n";
            if (e.InnerException != null)
            {
                message += "Inner exception:\r\n==============================" +
                           "Error Message: " + e.InnerException.Message +
                           "\r\n\r\nStack trace: " + e.InnerException.StackTrace +
                           "\r\n\r\nSource: " + e.InnerException.Source +
                           "\r\n\r\nSite: " + e.InnerException.TargetSite +
                           "\r\n\r\n";
            }

            message += "DateTime: " + DateTime.Now.ToString();

            Logger.Write(message, category.ToString());

            Logger.Writer.Dispose();
        }

        public static void Log(string message, Category category)
        {
            Logger.SetLogWriter(new LogWriterFactory().Create(), false);

            Logger.Write(message, category.ToString());

            Logger.Writer.Dispose();
        }

        public static void Log(string message, Category_EL category)
        {
            Logger.SetLogWriter(new LogWriterFactory().Create(), false);

            Logger.Write(message, category.ToString());

            Logger.Writer.Dispose();
        }
    }
}
