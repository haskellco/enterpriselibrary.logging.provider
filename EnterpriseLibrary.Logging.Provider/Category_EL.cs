﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseLibrary.Logging
{
    public enum Category_EL
    {
        VMSIndexEvent,
        VMSIndexError,
        VMSIndexGeneral,
        GeoCodingError,
        GeoCodingGeneral,
        VMSSearchUIError,
        VMSSearchUIGeneral
    }

    public enum Category
    {
        GeneralError,
        GeneralLog
    }
}
